import './certificate.css'

import sertifikat1 from '../../assets/sertifikat_binaracademy.png'
import sertifikat2 from '../../assets/sertifikat_vsga.png'

const Certificate = () => {
  const data = [
    {
      id: 1,
      title: 'Binar Academy Bootcamp',
      role: 'Full Stack Web Development',
      time: 'Jun 2022 - Jan 2023',
      image: sertifikat1,
      link: 'https://drive.google.com/file/d/1BXFHqLbJ2IffZWPV8hj6nDqon9K-4YqX/view?usp=sharing'
    },
    {
      id: 2,
      title: 'KOMINFO VSGA Digital Talent Scholarship',
      role: 'Junior Web Developer',
      time: '20 - 24 Feb 2023',
      image: sertifikat2,
      link: 'https://drive.google.com/file/d/1kSIObwdp6kCEkWfBBWGY4V2O0WvmgfGm/view?usp=sharing'
    }
  ]
  return (
    <section id="certificate">
      <h5>My Personal Certificate</h5>
      <h2>Certificate</h2>

      <div className="container container__certificate">
        {data.map(({ id, title, role, time, image, link }) => {
          return (
            <article key={id} className="certificateitems">
              <div className="certificateitems-image">
                <img src={image} alt={title} />
              </div>
              <div className="certificatetitle">
                <h3>{title}</h3>
              </div>
              <div className="certificatedesc">
                <h4>{role}</h4>
                <h5>{time}</h5>
              </div>
              <div className="certificateitems-cta">
                <a href={link} className="btn" target="_blank" rel="noreferrer">
                  View
                </a>
              </div>
            </article>
          )
        })}
      </div>
    </section>
  )
}

export default Certificate
