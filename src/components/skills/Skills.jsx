import './skills.css'

import skills from '../../assets/skills.png'
import skills600 from '../../assets/skills600.png'

const Skills = () => {
  return (
    <section id="skill">
      <h5>What are My Digital Skills?</h5>
      <h2>Skills</h2>

      <div className="container skills__container">
        <div className="skills">
          <h3>Digital Skills</h3>
          <img src={skills} alt="Skills" className="skills1" />
          <img src={skills600} alt="Skills" className="skills2" />
        </div>
      </div>
    </section>
  )
}

export default Skills
