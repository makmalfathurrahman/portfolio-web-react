import './portfolio.css'
import img1 from '../../assets/port1.png'
import img2 from '../../assets/port2.png'
import img3 from '../../assets/port3.png'

const data = [
  {
    id: 1,
    image: img1,
    title: 'Simple Single Page Application using React',
    gitlab: 'https://gitlab.com/makmalfathurrahman/simple-web-react',
    demo: 'https://simple-web-react.vercel.app/'
  },
  {
    id: 2,
    image: img2,
    title: 'Portfolio Website using React',
    gitlab: 'https://gitlab.com/makmalfathurrahman/portfolio-web-react',
    demo: 'https://makmalfathurrahman.vercel.app/'
  },
  {
    id: 3,
    image: img3,
    title: 'Single Page Application using Next.js',
    gitlab: 'https://gitlab.com/faufauzi94/binar-challenge-chapter-10-fsw-25',
    demo: 'https://challenge-11.vercel.app/'
  }
]

const Portfolio = () => {
  return (
    <section id="portfolio">
      <h5>My Recent Works</h5>
      <h2>Projects</h2>

      <div className="container container__portfolio">
        {data.map(({ id, image, title, gitlab, demo }) => {
          return (
            <article key={id} className="portofolioitems">
              <div className="portofolioitems-image">
                <img src={image} alt={title} />
              </div>
              <div className="portfoliotitle">
                <h3>{title}</h3>
              </div>
              <div className="portofolioitems-cta">
                <a
                  href={gitlab}
                  className="btn"
                  target="_blank"
                  rel="noreferrer"
                >
                  Gitlab
                </a>
                <a
                  href={demo}
                  className="btn btn-primary"
                  target="_blank"
                  rel="noreferrer"
                >
                  Live Demo
                </a>
              </div>
            </article>
          )
        })}
      </div>
    </section>
  )
}

export default Portfolio
