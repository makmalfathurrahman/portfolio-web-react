import './services.css'

import { BiCheck } from 'react-icons/bi'

const Services = () => {
  return (
    <section id="services">
      <h5>What I Offer?</h5>
      <h2>Services</h2>

      <div className="container services__container">
        {/* Web Design */}
        <article className="service">
          <div className="servicehead">
            <h3>Web Design</h3>
          </div>

          <ul className="servicelist">
            <li>
              <BiCheck className="servicelist-icon" size={30} />
              <p>
                Creating and producing the layout design of a website or web
                pages
              </p>
            </li>
            <li>
              <BiCheck className="servicelist-icon" size={30} />
              <p>
                Conceptualizing creative and responsive web design for all
                device
              </p>
            </li>
            <li>
              <BiCheck className="servicelist-icon" size={30} />
              <p>
                Designing attractive and responsive user interface using Figma
              </p>
            </li>
          </ul>
        </article>

        {/* Web Development */}
        <article className="service">
          <div className="servicehead">
            <h3>Web Development</h3>
          </div>

          <ul className="servicelist">
            <li>
              <BiCheck className="servicelist-icon" size={30} />
              <p>
                Ensures the functionality and stability of the website code on
                all devices
              </p>
            </li>
            <li>
              <BiCheck className="servicelist-icon" size={30} />
              <p>
                Optimizing sites for better performance and greater scalability
              </p>
            </li>
            <li>
              <BiCheck className="servicelist-icon" size={30} />
              <p>Working with the latest mechanisms and application code</p>
            </li>
          </ul>
        </article>
      </div>
    </section>
  )
}

export default Services
