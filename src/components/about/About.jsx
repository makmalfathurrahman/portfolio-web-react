import './about.css'

import me from '../../assets/itsme-ok.png'
// import binar from '../../assets/binar.png'
import { FiAward } from 'react-icons/fi'
import { FaRegFileCode } from 'react-icons/fa'

const About = () => {
  return (
    <section id="about">
      <h5>Get to know</h5>
      <h2>About Me</h2>

      <div className="container about__container">
        <div className="aboutme">
          <div className="aboutme_img">
            <img src={me} alt="About Image" />
          </div>
        </div>

        <div className="aboutcontent">
          <div className="aboutcards">
            <article className="cardsitem">
              <FiAward className="cardsicon" />
              <h5>Experience</h5>
              <small>Bootcamp Participant</small>
            </article>
            <article className="cardsitem">
              <FaRegFileCode className="cardsicon" />
              <h5>Projects</h5>
              <small>3+ Simple Projects</small>
            </article>
          </div>

          <p>
            Hello, I&apos;m a Junior Front-End Developer with Binar Academy
            certificate majoring in Full Stack Web Development for 6 months.
            Capable of designing websites with a variety of feature and
            developing websites with the support of the latest technology. Have
            great interest in programming, especially in Front-End Development.
          </p>

          <div className="aboutCTA">
            <a href="#contact" className="btn">
              Let&apos;s talk!
            </a>
          </div>
        </div>
      </div>
    </section>
  )
}

export default About
