import { HiOutlineHome } from 'react-icons/hi'
import { BiUser, BiMessageDetail } from 'react-icons/bi'
import { MdWorkOutline } from 'react-icons/md'
import { FiSettings } from 'react-icons/fi'
import { BsClipboardCheck } from 'react-icons/bs'

import './nav.css'

import { useState } from 'react'

const Nav = () => {
  const [active, setActive] = useState('#')

  return (
    <nav>
      <a
        href="#"
        onClick={() => setActive('#')}
        className={active === '#' ? 'active' : ''}
      >
        <HiOutlineHome />
      </a>
      <a
        href="#about"
        onClick={() => setActive('#about')}
        className={active === '#about' ? 'active' : ''}
      >
        <BiUser />
      </a>
      <a
        href="#skill"
        onClick={() => setActive('#skill')}
        className={active === '#skill' ? 'active' : ''}
      >
        <MdWorkOutline />
      </a>
      <a
        href="#certificate"
        onClick={() => setActive('#certificate')}
        className={active === '#certificate' ? 'active' : ''}
      >
        <BsClipboardCheck />
      </a>
      <a
        href="#services"
        onClick={() => setActive('#services')}
        className={active === '#services' ? 'active' : ''}
      >
        <FiSettings />
      </a>
      <a
        href="#contact"
        onClick={() => setActive('#contact')}
        className={active === '#contact' ? 'active' : ''}
      >
        <BiMessageDetail />
      </a>
    </nav>
  )
}

export default Nav
