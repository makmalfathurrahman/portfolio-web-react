import './contact.css'
import { HiOutlineMail } from 'react-icons/hi'
import { BsWhatsapp } from 'react-icons/bs'

// eslint-disable-next-line no-unused-vars
import React, { useRef } from 'react'
// eslint-disable-next-line no-unused-vars
import emailjs, { send } from '@emailjs/browser'

const Contact = () => {
  const form = useRef()

  const sendEmail = (e) => {
    e.preventDefault()

    emailjs
      .sendForm(
        'service_u9fj3us',
        'template_x42b0uh',
        form.current,
        'qICP3GO8BYosmdWyj'
      )
      .then(
        (result) => {
          console.log(result.text)
        },
        (error) => {
          console.log(error.text)
        }
      )

    e.target.reset()
  }
  return (
    <section id="contact">
      <h5>Get in Touch</h5>
      <h2>Contact Me</h2>

      <div className="container contact__container">
        <div className="contactoptions">
          <article className="optionsitem">
            <HiOutlineMail className="optionsitem-icon" />
            <h4>Email</h4>
            <h5>makmalfath@gmail.com</h5>
            <a
              href="mailto:makmalfath@gmail.com"
              target="_blank"
              rel="noreferrer"
            >
              Send a message
            </a>
          </article>
          <article className="optionsitem">
            <BsWhatsapp className="optionsitem-icon" />
            <h4>WhatsApp</h4>
            <h5>+62-812-XXXX-XXXX</h5>
            <a
              href="https://wa.me/6281224044314"
              target="_blank"
              rel="noreferrer"
            >
              Send a message
            </a>
          </article>
        </div>

        {/* FORM */}
        <form ref={form} onSubmit={sendEmail}>
          <input type="text" name="name" placeholder="Full Name" required />
          <input type="email" name="email" placeholder="Email" required />
          <textarea
            name="message"
            rows="7"
            placeholder="Message"
            required
          ></textarea>
          <div className="button">
            <button type="submit" className="btn btn-primary">
              Send Message
            </button>
          </div>
        </form>
      </div>
    </section>
  )
}

export default Contact
