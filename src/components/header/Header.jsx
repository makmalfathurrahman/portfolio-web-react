import './header.css'
import HeaderCTA from './HeaderCTA.jsx'
import me from '../../assets/itsme.png'
import HeaderSocmed from './HeaderSocmed'

const Header = () => {
  return (
    <header>
      <div className="container header__container">
        <h5>Hello, I&apos;m</h5>
        <h1>Muhammad Akmal Fathurrahman</h1>
        <h5 className="text-light">Junior Front-End Developer</h5>
        <HeaderCTA />
        <HeaderSocmed />

        <div className="me">
          <img src={me} alt="me" />
        </div>

        <a href="#contact" className="scrolldown">
          Scroll Down
        </a>
      </div>
    </header>
  )
}

export default Header
