const HeaderCTA = () => {
  return (
    <div className="headercta">
      <a
        href="https://drive.google.com/file/d/1wVcG68DUo4RKdLhY0BILJl95Zt6-SmL2/view?usp=sharing"
        target="_blank"
        rel="noreferrer"
        className="btn btn-primary"
      >
        View CV
      </a>
      <a href="#contact" className="btn">
        Let&apos;s talk!
      </a>
    </div>
  )
}

export default HeaderCTA
