import { BsLinkedin } from 'react-icons/bs'
import { RiGitlabFill } from 'react-icons/ri'

const HeaderSocmed = () => {
  return (
    <div className="headersocmed">
      <a
        href="https://www.linkedin.com/in/makmalfathurrahman/"
        target="_blank"
        rel="noreferrer"
      >
        <BsLinkedin />
      </a>
      <a
        href="https://gitlab.com/makmalfathurrahman"
        target="_blank"
        rel="noreferrer"
      >
        <RiGitlabFill />
      </a>
    </div>
  )
}

export default HeaderSocmed
