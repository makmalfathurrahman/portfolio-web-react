import './App.css'

import About from './components/about/About'
import Certificate from './components/certificate/Certificate'
import Contact from './components/contact/Contact'
import Footer from './components/footer/Footer'
import Header from './components/header/Header'
import Nav from './components/nav/Nav'
import Portfolio from './components/portfolio/Portfolio'
import Services from './components/services/Services'
import Skills from './components/skills/Skills'

function App() {
  return (
    <>
      <Header />
      <Nav />
      <About />
      <Skills />
      <Certificate />
      <Services />
      <Portfolio />
      <Contact />
      <Footer />
    </>
  )
}

export default App
